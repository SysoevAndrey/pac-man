#include "pack.h"

int draw_packman(int i, xv_t *xv) {
    XFillArc(xv->dpy, xv->box, xv->gc[i], packman.x - packman.radius + RAD, 
             packman.y - packman.radius + RAD,
             2 * packman.radius - 2*RAD, 2 * packman.radius- 2*RAD, 
             64 * (packman.center + packman.angle), 
             64 * (360 - 2 * packman.angle));
    return(0);
}

int change_angle() {
    if (packman.angle_move > 0 && packman.angle < 45) {
        packman.angle += packman.angle_move;
    } else if (packman.angle_move > 0 && packman.angle >= 45) {
        packman.angle_move *= -1;
        packman.angle += packman.angle_move;
    } else if (packman.angle_move < 0 && packman.angle > 0) {
        packman.angle += packman.angle_move;
    } else if (packman.angle_move < 0 && packman.angle <= 0) {
        packman.angle_move *= -1;
        packman.angle += packman.angle_move;
    }
    return 0;
}

int move_packman(xv_t *xv) {
    int i = packman.y / dBH;
    int ih = (packman.y + packman.radius - STEP2) / dBH;
    int il = (packman.y - packman.radius + STEP2) / dBH;
    int j = packman.x / dBW;
    int jh = (packman.x + packman.radius - STEP2) / dBW;
    int jl = (packman.x - packman.radius + STEP2) / dBW;
    if (packman.x_speed > 0 && ((packman.x + packman.radius + packman.x_speed) < BW)) {
        if (!(xv->map[i][j + 1] == 1) && !(xv->map[il][j + 1] == 1) && !(xv->map[ih][j + 1] == 1)) {
            packman.x += packman.x_speed;
        } else if ((packman.x + packman.radius + packman.x_speed + 1) < (j + 1) * dBH) {
            packman.x += packman.x_speed;
        }
          else if ((packman.x + packman.radius) < (j+1)*dBH) {
            packman.x = (j+1)*dBH - packman.radius;
          }
    } else if (packman.x_speed > 0 && (packman.x + packman.radius + 2) < BW) {
        packman.x = BW - packman.radius;
    }
    
    if (packman.y_speed > 0 && (packman.y + packman.radius + packman.y_speed) < BH) {
        if (!(xv->map[i + 1][j] == 1) && !(xv->map[i + 1][jh] == 1) && !(xv->map[i + 1][jl]==1)) {
            packman.y += packman.y_speed;
        } else if ((packman.y + packman.radius + packman.y_speed) < (i + 1) * dBH) {
            packman.y += packman.y_speed;
        }
          else if ((packman.y + packman.radius) < (i+1)*dBH) {
            packman.y = (i+1)*dBH - packman.radius;
          }
    } else if (packman.y_speed > 0 && (packman.y + packman.radius) < BH) {
        packman.y = BH - packman.radius;
    }

    if (packman.x_speed < 0 && (packman.x - packman.radius + packman.x_speed) > 0) {
        if (!(xv->map[i][j - 1] == 1) && !(xv->map[il][j - 1] == 1) && !(xv->map[ih][j - 1] == 1)) {
            packman.x += packman.x_speed;
        } else if ((packman.x - packman.radius + packman.x_speed) > (j) * dBW) {
            packman.x += packman.x_speed;
        }
    } else if (packman.x_speed < 0 && (packman.x - packman.radius) > 0) {
        packman.x = packman.radius;
    }
    
    if (packman.y_speed < 0 && (packman.y - packman.radius + packman.y_speed) > 0) {
        if (!(xv->map[i - 1][j] == 1) && !(xv->map[i - 1][jh] == 1) && !(xv->map[i - 1][jl] == 1)) {
            packman.y += packman.y_speed;
        } else if ((packman.y - packman.radius + packman.y_speed) > (i) * dBW) {
            packman.y += packman.y_speed;
        }
    } else if (packman.y_speed < 0 && (packman.y - packman.radius) > 0) {
        packman.y = packman.radius;
    }
    packman.score += have_food(xv);
    change_angle();
    
    return 0;
}

int have_food(xv_t *xv) {
    int i = packman.y / dBH;
    int j = packman.x / dBW;
    int dif_x = dBW * j + dBW * 0.5 - packman.x;
    if (dif_x < 0) {
        dif_x *= -1;
    }
    int dif_y = dBH * i + dBH * 0.5 - packman.y;
    if (dif_y < 0) {
        dif_y *= -1;
    }
    if ((dif_x < FOOD_RADIUS) && (dif_y < FOOD_RADIUS)) {
        xv->map[i][j] = 0;
        return 1;
    }
    return 0;
}
