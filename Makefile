.PHONY: all clean
  
EXEC = pack
LIBS = -L /opt/local/lib -lX11
INCLUDES = -I/opt/local/include
CC = cc
CFLAGS = -Wall $(INCLUDES) $(LIBS) -ggdb3
OBJECTS = main.c render_map.c render_pac.c win.c
INCLUDE_FILES = test.h

all :
	$(CC) -o $(EXEC) $(OBJECTS) $(CFLAGS)

clean :
	$(RM) pack
