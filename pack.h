#ifndef PACK_H
#define PACK_H

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/keysymdef.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#define COLOR_BACK 0x000080
#define COLOR_WALL 0x7B68EE
#define COLOR_CATCH 0xFFD700
#define COLOR_FOOD 0xFFFACD
#define SPEED 1
#define STEP 1
#define STEP2 1
#define FOOD_RADIUS 7
#define SIZE 30
#define MAX_FOOD 400
#define MIN_ST 2
#define PRO 3
#define ST 6
#define RAD 1

#define CLOCK_LIMIT 15000

#define dBW 25
#define dBH 25
#define BW 750
#define BH 800

typedef struct {
    Display* dpy;
    GC gc[4];
    int food;
    int X0;
    int Y0;
    unsigned int NX;
    unsigned int NY;
    Window root;
    Window box;
    int map[SIZE][SIZE];
} xv_t;

struct packman {
    int x;
    int y;
    int radius;
    int center;
    int old_center;
    int angle;
    int angle_move;
    int x_speed;
    int y_speed;
    int score;
} packman;

int begin(xv_t*);
int have_food(xv_t*);
int draw_packman(int, xv_t*);
int generate_map(xv_t*);
int change_angle();
int move_packman(xv_t*);
int window(xv_t*, XRectangle*);
int dispatch(xv_t*);
int keydrive(XEvent*, xv_t*);
int draw_walls(xv_t*);
int is_good_map(xv_t*);
int map_modifying(xv_t *xv, int ix, int iy);

#endif // PACK_H
