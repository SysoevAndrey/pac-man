#include "pack.h"

int main(int argc, char* argv[]) {
    XRectangle cell;

    xv_t  xv;
    xv.food = 0;
    xv.X0 = 0;
    xv.Y0 = 0;
    xv.NX = 1;
    xv.NY = 1;

    XParseGeometry(argv[1], &xv.X0, &xv.Y0, &xv.NX, &xv.NY);

    xv.dpy = XOpenDisplay(NULL);

    if (window(&xv, &cell) > 0) {
        return(1);
    }

    begin(&xv);
    packman.x_speed = 0;
    packman.y_speed = 0;
    packman.radius = dBH/2;
    packman.center = 0;
    packman.old_center = 0;
    packman.angle = 45;
    packman.angle_move = 10;
    packman.score = 0;
    dispatch(&xv);

    XDestroySubwindows(xv.dpy, xv.root);
    XDestroyWindow(xv.dpy, xv.root);
    XCloseDisplay(xv.dpy);
}

