#include "pack.h"

int window(xv_t *xv, XRectangle *cell) {
    int src;
    int depth;
    XSetWindowAttributes attr;
    unsigned long amask;
    XSizeHints hint;
    XFontStruct* fn;
    char* fontname = "9x15";

    src = DefaultScreen(xv->dpy);
    depth = DefaultDepth(xv->dpy, src);
    xv->gc[0] = DefaultGC(xv->dpy, src);
    XSetForeground(xv->dpy, xv->gc[0], COLOR_WALL);

    if (!(fn = XLoadQueryFont(xv->dpy, fontname)))
        return(puts("Incorrect FontStruct id"));
    XSetFont(xv->dpy, xv->gc[0], fn->fid);

    cell->width = fn->max_bounds.width;
    cell->height = fn->max_bounds.ascent + fn->max_bounds.descent;
    cell->x = (BW - fn->max_bounds.width) / 2;
    cell->y = BH/2 + (fn->max_bounds.ascent - fn->max_bounds.descent)/2;

    amask = (CWOverrideRedirect | CWBackPixel | CWEventMask);
    attr.override_redirect = False;
    attr.background_pixel = WhitePixel(xv->dpy, src);
    attr.event_mask = (ButtonPressMask | ButtonReleaseMask | KeyPressMask);
    xv->root = XCreateWindow(xv->dpy, DefaultRootWindow(xv->dpy), xv->X0, xv->Y0, BW, BH, 1, depth,
                         InputOutput, CopyFromParent, amask, &attr);

    hint.flags = (PMinSize | PMaxSize | PPosition);
    hint.min_width = hint.max_width = BW;
    hint.min_height = hint.max_height = BH;
    hint.x = xv->X0;
    hint.y = xv->Y0;
    XSetNormalHints(xv->dpy, xv->root, &hint);

    amask = (CWOverrideRedirect | CWBackPixel | CWEventMask);
    attr.override_redirect = True;
    attr.background_pixel = COLOR_BACK;
    attr.event_mask = (ExposureMask);
    xv->box = XCreateWindow(xv->dpy, xv->root, xv->X0, xv->Y0, BW, BH, 1,  depth, InputOutput, 
                        CopyFromParent, amask, &attr);

    XMapWindow(xv->dpy, xv->root);
    XMapSubwindows(xv->dpy, xv->root);
    XMapSubwindows(xv->dpy, xv->box);
    XStoreName(xv->dpy, xv->root, "PAC-MAN PRO");

    xv->gc[1] = XCreateGC(xv->dpy, xv->root, 0, 0);
    XCopyGC(xv->dpy, xv->gc[0], GCFont, xv->gc[1]);
    XSetForeground(xv->dpy, xv->gc[1], COLOR_BACK);

    xv->gc[2] = XCreateGC(xv->dpy, xv->root, 0, 0);
    XCopyGC(xv->dpy, xv->gc[0], GCFont, xv->gc[2]);
    XSetForeground(xv->dpy, xv->gc[2], COLOR_CATCH);

    xv->gc[3] = XCreateGC(xv->dpy, xv->root, 0, 0);
    XCopyGC(xv->dpy, xv->gc[0], GCFont, xv->gc[2]);
    XSetForeground(xv->dpy, xv->gc[3], COLOR_FOOD);

    return(0);
}
int dispatch(xv_t *xv) {
    XEvent event;
    long emask = (ButtonPressMask | ButtonReleaseMask | KeyPressMask | ExposureMask);
    int flag = 0;
    clock_t ck = clock();
    while(!flag) {
        if ((clock() - ck) > CLOCK_LIMIT) {
            XCheckMaskEvent(xv->dpy, emask, &event);
            switch(event.type) {
                case KeyPress:
                    flag = keydrive(&event, xv);
                    break;
                default:
                    break;
            }
            ck = clock();
            if (packman.center != packman.old_center) {
                XFillArc(xv->dpy, xv->box, xv->gc[1], packman.x - packman.radius,
                         packman.y - packman.radius, 2 * packman.radius,
                         2 * packman.radius, 64 * (packman.old_center + packman.angle),
                         64 * (360 - 2 * packman.angle));
            }
            draw_packman(1, xv);
            draw_walls(xv);
            move_packman(xv);
            draw_packman(2, xv);
        }
    }
    return(0);
}

int keydrive(XEvent* ev, xv_t *xv) {
    int flag = 0;
    KeySym sym;
    XLookupString((XKeyEvent*)ev, NULL, 0, &sym, NULL);
    int i = packman.y / dBH;
    int ih = (packman.y + packman.radius - STEP2) / dBH;
    int il = (packman.y - packman.radius + STEP2) / dBH;
    int j = packman.x / dBW;
    int jh = (packman.x + packman.radius - STEP2) / dBW;
    int jl = (packman.x - packman.radius + STEP2) / dBW;
    switch(sym) {
        case XK_Down:
            if (!(xv->map[i + 1][j] == 1) && !(xv->map[i + 1][jh] == 1) 
                && !(xv->map[i + 1][jl] == 1)) {
                packman.x_speed = 0;
                packman.y_speed = SPEED;
                packman.old_center = packman.center;
                packman.center = 270;
            } else if ((packman.y + packman.radius + packman.y_speed + STEP) < (i + 1)
                       * dBH) {
                packman.x_speed = 0;
                packman.y_speed = SPEED;
                packman.old_center = packman.center;
                packman.center = 270;
            }
            break;
        case XK_Up:
            if (!(xv->map[i - 1][j] == 1) && !(xv->map[i - 1][jh] == 1) 
                && !(xv->map[i - 1][jl] == 1)) {
                packman.x_speed = 0;
                packman.y_speed = -SPEED;
                packman.old_center = packman.center;
                packman.center = 90;
            } else if ((packman.y - packman.radius + packman.y_speed - STEP) > i * dBW) {
                packman.x_speed = 0;
                packman.y_speed = -SPEED;
                packman.old_center = packman.center;
                packman.center = 90;
            }
            break;
        case XK_Left:
            if (!(xv->map[i][j - 1] == 1) && !(xv->map[il][j - 1] == 1) 
                && !(xv->map[ih][j - 1] == 1)) {
                packman.x_speed = -SPEED;
                packman.y_speed = 0;
                packman.old_center = packman.center;
                packman.center = 180;
            } else if ((packman.x - packman.radius + packman.x_speed - STEP) > (j) 
                       * dBW) {
                packman.x_speed = -SPEED;
                packman.y_speed = 0;
                packman.old_center = packman.center;
                packman.center = 180;
            }
            break;
        case XK_Right:
            if (!(xv->map[i][j + 1] == 1) && !(xv->map[il][j + 1] == 1) 
                && !(xv->map[ih][j + 1] == 1)) {
                packman.x_speed = SPEED;
                packman.y_speed = 0;
                packman.old_center = packman.center;
                packman.center = 0;
            } else if ((packman.x + packman.radius + packman.x_speed + STEP) < (j + 1) 
                       * dBH) {
                packman.x_speed = SPEED;
                packman.y_speed = 0;
                packman.old_center = packman.center;
                packman.center = 0;
            }
            break;
        case XK_Escape:
            flag = 1;
            break;
        default:
            break;
    }
    return(flag);
}

int begin(xv_t *xv) {
    srand(time(NULL));
    for(int i = 0; i < SIZE; ++i) {
        for(int j = 0; j < SIZE; ++j) {
            xv->map[i][j] = 0;
        }
    }
    xv->food = 0;
    generate_map(xv);
    while (is_good_map(xv)) {
        for(int i = 0; i < SIZE; ++i) {
            for(int j = 0; j < SIZE; ++j) {
                xv->map[i][j] = 0;
            }
        }
        xv->food = 0;
        generate_map(xv);
    }

    for(int i = 0; i < SIZE; ++i) {
        for(int j = 0; j < SIZE; ++j) {
            if (!xv->map[i][j]) {
                xv->map[i][j] = 1;
            }
        }
    }
    return 0;
}
