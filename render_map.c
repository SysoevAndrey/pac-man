#include "pack.h"

int is_good_map(xv_t *xv) {
    int px = packman.y / dBW;
    int py = packman.x / dBW; 
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            if(xv->map[i][j] && ((xv->map[i - 1][j] && xv->map[i][j - 1] && xv->map[i - 1][j - 1])       
               || (xv->map[i + 1][j] && xv->map[i][j - 1] && xv->map[i + 1][j - 1])
               || (xv->map[i + 1][j] && xv->map[i][j + 1] && xv->map[i + 1][j + 1]) 
               || (xv->map[i - 1][j] && xv->map[i][j + 1] && xv->map[i - 1][j + 1]))) {
                return 1;
            }
        }
    }
    if (xv->map[px-1][py]==0 && xv->map[px][py-1]==0 && xv->map[px+1][py]==0 && xv->map[px][py+1]==0) {
        return 1;
    }
    for(int i = 0; i < SIZE; ++i) {
        for(int j = 0; j < SIZE; ++j) {
            if (xv->map[i][j] && !xv->map[i-1][j] && !xv->map[i][j-1]
                && !xv->map[i+1][j] && !xv->map[i][j+1]) {
                return 1;
            }
        }
    }
    return 0;
}

int map_modifying(xv_t *xv, int ix, int iy) {
    int step = 0;
    int i = 0;
    xv->map[ix][iy] = 2;
    if (xv->food < MAX_FOOD) {
        if (ix > 2 && ix < 27) {
            if (!xv->map[ix - 1][iy] && rand()%PRO && (!xv->map[ix - 1][iy - 1] 
                || !xv->map[ix][iy - 1]) && (!xv->map[ix - 1][iy + 1] || !xv->map[ix][iy + 1])) {
                step = MIN_ST + rand()%ST;
                for(i = 1; i <= step; ++i) {
                    if ((!xv->map[ix - i][iy] && (!xv->map[ix - i][iy - 1] || !xv->map[ix][iy - 1]) &&
                        (!xv->map[ix - i][iy + 1] || !xv->map[ix][iy + 1])) && xv->food < MAX_FOOD) {
                        xv->map[ix - i][iy] = 2;
                        xv->food++;
                        if (xv->map[ix - i - 1][iy] || xv->map[ix - i][iy - 1] 
                            || xv->map[ix - i][iy + 1]) {
                            break;
                        }
                        if (ix - i < 2) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!xv->map[ix - i][iy] && xv->food < MAX_FOOD) {
                    map_modifying(xv, ix - i + 1, iy);
                } else if (i != 1) {
                    map_modifying(xv, ix - i + 1, iy);
                }
            }
            if (!xv->map[ix + 1][iy] && rand()%PRO && (!xv->map[ix + 1][iy - 1] 
                || !xv->map[ix][iy - 1]) && (!xv->map[ix + 1][iy + 1] || !xv->map[ix][iy + 1])) {
                step = MIN_ST + rand()%ST;
                for(i = 1; i <= step; ++i) {
                    if ((!xv->map[ix + i][iy] && (!xv->map[ix + i][iy - 1] || !xv->map[ix][iy - 1]) &&
                        (!xv->map[ix + i][iy + 1] || !xv->map[ix][iy + 1])) && xv->food < MAX_FOOD) {
                        xv->map[ix + i][iy] = 2;
                        xv->food++;
                        if (xv->map[ix + i + 1][iy] || xv->map[ix + i][iy - 1] 
                            || xv->map[ix + i][iy + 1]) {
                            break;
                        }
                        if (ix + i > 27) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!xv->map[ix - i][iy] && xv->food < MAX_FOOD) {
                    map_modifying(xv, ix + i - 1, iy);
                } else if (i != 1) {
                    map_modifying(xv, ix + i - 1, iy);
                }
            }
        }
        if (iy > 2 && iy < 27) {
            if (!xv->map[ix][iy - 1] && rand()%PRO && (!xv->map[ix - 1][iy - 1] 
                || !xv->map[ix - 1][iy]) && (!xv->map[ix + 1][iy] || !xv->map[ix + 1][iy - 1])) {
                step = MIN_ST + rand()%ST;
                for(i = 1; i <= step; ++i) {
                    if ((!xv->map[ix][iy - i] && (!xv->map[ix - 1][iy - i] || !xv->map[ix - i][iy]) &&
                        (!xv->map[ix + 1][iy] || !xv->map[ix + 1][iy - i])) && xv->food < MAX_FOOD) {
                        xv->map[ix][iy - i] = 2;
                        xv->food++;
                        if (xv->map[ix][iy - i - 1] || xv->map[ix - 1][iy - i] 
                            || xv->map[ix + 1][iy - i]) {
                            break;
                        }
                        if (iy - i < 2) {
                            break;
                        } 
                    } else {
                        break;
                    }
                }
                if (!xv->map[ix - i][iy] && xv->food < MAX_FOOD) {
                    map_modifying(xv, ix, iy - i + 1);
                } else if (i != 1) {
                    map_modifying(xv, ix, iy - i + 1);
                }
            }
            if (!xv->map[ix][iy + 1] && rand()%PRO && (!xv->map[ix - 1][iy + 1] 
                || !xv->map[ix - 1][iy]) && (!xv->map[ix + 1][iy] || !xv->map[ix + 1][iy + 1])) {
                step = MIN_ST + rand()%ST;
                for(i = 1; i <= step; ++i) {
                    if ((!xv->map[ix][iy + i] && (!xv->map[ix - 1][iy + i] || !xv->map[ix - 1][iy]) &&
                        (!xv->map[ix + 1][iy] || !xv->map[ix + 1][iy + i])) && xv->food < MAX_FOOD) {
                        xv->map[ix][iy + i] = 2;
                        xv->food++;
                        if (xv->map[ix][iy + i + 1] || xv->map[ix - 1][iy + i] 
                            || xv->map[ix + 1][iy + i]) {
                            break;
                        }
                        if (iy + i > 27) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!xv->map[ix - i][iy] && xv->food < MAX_FOOD) {
                    map_modifying(xv, ix, iy + i - 1);
                } else if (i != 1) {
                    map_modifying(xv, ix, iy + i - 1);
                }
            }
        }
    }
    return 0;
}

int generate_map(xv_t *xv) {
    int init_x = rand()%10;
    int init_y = rand()%10;
    xv->map[10+init_x][10+init_y] = 2;
    xv->food++;
    packman.y = (10 + init_x)*dBW + dBW / 2;
    packman.x = (10 + init_y)*dBH + dBH / 2;
    map_modifying(xv, 10+init_x, 10+init_y);
    while (xv->food < MAX_FOOD) {
        int ox = rand()%30;
        int oy = rand()%30;
        if (xv->map[ox][oy]) {
            map_modifying(xv, ox, oy);
        }
    }
    return 0;
}

int draw_walls(xv_t *xv) {
    for(int i = 0; i < SIZE; ++i) {
        for(int j = 0; j < SIZE; ++j) {
            if (xv->map[j][i] == 1) {
                XFillRectangle(xv->dpy, xv->box, xv->gc[0], i * dBH + 0.1 * dBH,
                               j * dBW + 0.1 * dBW, 0.8 * dBH, 0.8 * dBH);
            } else if (xv->map[j][i] == 2) {
                XFillArc(xv->dpy, xv->box, xv->gc[3], i * dBH + 0.4 * dBH,
                         j * dBW + 0.4 * dBW, 0.2 * dBH, 0.2 * dBW, 0, 360 * 64);
            }
        }
    }
    return 0;
}
